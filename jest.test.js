/*
 * @Author: wanghuang
 * @Date: 2021-09-16 13:26:33
 * @LastEditTime: 2021-09-16 14:00:41
 * @LastEditors: wanghuang
 * @Description: 单元测试
 */

test('test common matcher', () => {
    expect(1 + 1).toBe(2)
    expect(3 + 4).not.toBe(1)
})

test('test to be true or false', () => {
    expect(1).toBeTruthy()
    expect(0).toBeFalsy()
})

test('test greater or less', () => {
    expect(1).toBeGreaterThan(0)
    expect(-2).toBeLessThan(-1)
})

test('test object', () => {
    expect({name:'wanghuang'}).toEqual({name:'wanghuang'})
})