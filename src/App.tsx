/*
 * @Author: wanghuang
 * @Date: 2021-09-13 13:08:31
 * @LastEditTime: 2021-09-28 14:22:14
 * @LastEditors: wanghuang
 * @Description: 
 */
import React from 'react';
import Button from './components/Button/button';
import Menu from './components/Menu';
import MenuItem from './components/Menu/menuItem';
import SubMenu from './components/Menu/subMenu';
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import Icon from './components/Icon/icon';
library.add(fas)
const App: React.FC = () => {
  return (
    <div className="App">
      <Icon icon='arrow-right' theme='success' size={'10x'} />
      <header className="App-header">
        <h1>hello typescript</h1>
        <h2>hello typescript</h2>
        <h3>hello typescript</h3>
        <code>
          const a = 'b';
        </code>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <br />
        <Button disabled>禁用了</Button>
        <Button btnType={'primary'}>primary</Button>
        <Button btnType={'danger'} size={'lg'}>大号primary</Button>
        <Button btnType={'link'} target="_blank" href='https://overlay-tech.com/?ref=article-figmaconcept'>正常link</Button>
        <Button btnType={'link'} disabled>禁用link</Button>
      </header>
      <Menu mode={'horizontal'} defaultIndex={'0'} onSelect={(idx) => alert(idx)}>
        <MenuItem>这是第一个MenuItem</MenuItem>
        <MenuItem disabled>这是第二个MenuItem</MenuItem>
        <MenuItem>这是第三个MenuItem</MenuItem>
        <SubMenu title='下拉菜单'>
          <MenuItem>1111</MenuItem>  
          <MenuItem>wwww</MenuItem>  
          <MenuItem>vvvv</MenuItem>  
        </SubMenu>
      </Menu>
    </div>
  );
}

export default App;
