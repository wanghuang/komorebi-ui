/*
 * @Author: wanghuang
 * @Date: 2021-09-29 11:20:23
 * @LastEditTime: 2021-09-29 16:03:15
 * @LastEditors: wanghuang
 * @Description: button.stories.tsx
 */
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import Button from './button';

export default {
    title: 'Example/Button',
    component: Button,
    argTypes: {
        backgroundColor: { control: 'color' },
    },
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
    btnType: 'primary',
    size: 'sm',
    children: 'primary'
};

export const Success = Template.bind({});
Success.args = {
    size: 'sm',
    btnType: 'success',
    children: 'success'
};

export const Danger = Template.bind({});
Danger.args = {
    size: 'sm',
    btnType: 'danger',
    children: 'danger'
};

export const Warning = Template.bind({});
Warning.args = {
    size: 'sm',
    btnType: 'warning',
    children: 'warning'
};

export const Link = Template.bind({});
Link.args = {
    size: 'lg',
    btnType: 'link',
    children: 'link'
};
