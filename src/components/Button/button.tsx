/*
 * @Author: wanghuang
 * @Date: 2021-09-13 13:08:31
 * @LastEditTime: 2021-09-29 15:58:26
 * @LastEditors: wanghuang
 * @Description: Button组件
 */
import React, { FC, ButtonHTMLAttributes, AnchorHTMLAttributes} from 'react';
import classNames from 'classnames';

export type ButtonSize = 'lg' | 'sm'
export type ButtonType = 'default' | 'primary' | 'success' | 'danger' | 'warning' | 'link'

interface BaseButtonProps {
    /** 自定义类名 */
    className?: string,
    /** 设置按钮的尺寸 */
    size?: ButtonSize,
    /** 跳转，指定此属性 button 的行为和 a 链接一致 */
    href?: string,
    /** 设置按钮类型 */
    btnType?: ButtonType,
    /** 按钮失效状态 */
    disabled?: boolean,
    children: React.ReactNode
}

type NativeButtonProps = BaseButtonProps & ButtonHTMLAttributes<HTMLElement>;
type AnchorButtonProps = BaseButtonProps & AnchorHTMLAttributes<HTMLElement>;
export type ButtonProps = Partial<NativeButtonProps & AnchorButtonProps>;
/**
 * 按钮用于开始一个即时操作
 * 
 * ### 引用方法
 * 
 * ~~~js
 * import { Button } from 'Kmorebi-ui'
 * ~~~
 */
export const Button: FC<ButtonProps> = (props) => {
    const { className, btnType, disabled, size, href, children, ...restProps } = props;
    const classes = classNames('btn', className, {
        [`btn-${btnType}`]: btnType,
        [`btn-${size}`]: size,
        'disabled': (btnType === 'link') && disabled//表达式结果为true时添加上disabled属性
    })
    if (btnType === 'link' && href) {
        return <a
            className={classes}
            href={href}
            {...restProps}
        >
            {children}
        </a>
    } else {
        return <button
            className={classes}
            disabled={disabled}
            {...restProps}
        >
            {children}
        </button>
    }
}
Button.defaultProps = {
    disabled: false,
    btnType: 'default'
}
export default Button;