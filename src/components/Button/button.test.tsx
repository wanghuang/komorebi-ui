/*
 * @Author: wanghuang
 * @Date: 2021-09-16 14:08:33
 * @LastEditTime: 2021-09-16 15:09:39
 * @LastEditors: wanghuang
 * @Description: 
 */
import React from 'react';
import { render, fireEvent } from "@testing-library/react";
import Button, { ButtonProps } from './button';
const defaultProps = {
    onClick: jest.fn()
}
// test('the first test case', () => {
//     const wrapper = render(<Button>haha</Button>);
//     const element = wrapper.queryByText('haha');
//     expect(element).toBeTruthy();
// })
const testProps: ButtonProps = {
    btnType: 'primary',
    size: 'lg',
    className: 'testclass'
}
const disabledProps: ButtonProps = {
    disabled: true,
    onClick: jest.fn(),
  }
describe('test button component', () => {
    it('should render the default button', () => {
        const wrapper = render(<Button>haha</Button>);
        const element = wrapper.getByText('haha');
        expect(element).toBeInTheDocument();
        expect(element.tagName).toEqual('BUTTON');
        expect(element).toHaveClass('btn btn-default');
    })
    it ('should render the button based on difference props', () => {
        const wrapper = render(<Button {...testProps}>haha</Button>)
        const element = wrapper.getByText('haha')
        expect(element).toBeInTheDocument()
        expect(element).toHaveClass('btn-primary btn-lg testclass')
    })
    it('should render the button that acts like a link when btnType is equal to link', () => {
        const wrapper = render(<Button btnType='link' href={'http://www.baidu.com'}>link</Button>)
        const element = wrapper.getByText('link')
        expect(element).toBeInTheDocument()
        expect(element.tagName).toEqual('A');
        expect(element).toHaveClass('btn btn-link')
    })
    it('should render disabled button when disabled set to true', () => {
        const wrapper = render(<Button {...disabledProps}>hell</Button>)
        const element = wrapper.getByText('hell') as HTMLButtonElement
        expect(element).toBeInTheDocument()
        expect(element.disabled).toBeTruthy()
        fireEvent.click(element)
        expect(disabledProps.onClick).not.toHaveBeenCalled()
      })
})