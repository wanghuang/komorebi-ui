/*
 * @Author: wanghuang
 * @Date: 2021-09-27 16:09:40
 * @LastEditTime: 2021-09-27 16:10:51
 * @LastEditors: wanghuang
 * @Description: transition component
 */
import Transition from './transition';

export default Transition