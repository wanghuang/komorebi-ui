/*
 * @Author: wanghuang
 * @Date: 2021-09-28 12:49:21
 * @LastEditTime: 2021-09-28 13:06:48
 * @LastEditors: wanghuang
 * @Description: Icon component
 */
import React from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon, FontAwesomeIconProps } from '@fortawesome/react-fontawesome';

//  将系统主题添加到Icon中
export type ThemeProps = 'primary' | 'secondary' | 'success' | 'info' | 'warning' | 'danger' | 'light' | 'dark';

export interface IconProps extends FontAwesomeIconProps {
    theme?: ThemeProps
}

const Icon: React.FC<IconProps> = (props) => {
    const { theme, className, ...restProps } = props;
    const classes = classNames('komorebi-icon', className, {
        [`icon-${theme}`]: theme
    })
    return <FontAwesomeIcon className={classes} {...restProps} />
}

export default Icon;
