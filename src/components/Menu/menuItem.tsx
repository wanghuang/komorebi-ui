/*
 * @Author: wanghuang
 * @Date: 2021-09-25 13:20:16
 * @LastEditTime: 2021-09-27 14:27:07
 * @LastEditors: wanghuang
 * @Description: menuItem component
 */
import React, { useContext } from "react";
import classNames from "classnames";
import { MenuContext } from "./menu";

export interface MenuItemProps {
    index?: string,
    className?: string,
    disabled?: boolean,
    style?: React.CSSProperties
}
const MenuItem: React.FC<MenuItemProps> = (props) => {
    const { index, className, disabled, style, children } = props;
    const { currentIdx, onSelect } = useContext(MenuContext);
    const classes = classNames('menu-item', className, {
        'is-disabled': disabled,
        'is-active': currentIdx === index
    })
    const handleItemClick = () => {
        onSelect && !disabled && typeof index === 'string' && onSelect(index)
    }
    return <li className={classes} style={style} onClick={handleItemClick}>
        {children}
    </li>
}
MenuItem.displayName = 'MenuItem';
export default MenuItem;