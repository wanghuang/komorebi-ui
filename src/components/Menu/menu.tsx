/*
 * @Author: wanghuang
 * @Date: 2021-09-16 15:22:57
 * @LastEditTime: 2021-09-27 14:51:01
 * @LastEditors: wanghuang
 * @Description: Menu Component
 */
import React, { useState, createContext, Children } from "react";
import classNames from "classnames";
import { MenuItemProps } from './menuItem';
type MenuMode = 'vertical' | 'horizontal';
type selectCallback = (selectedIndex: string) => void;

export interface MenuProps {
    className?: string,
    defaultIndex?: string,
    defaultOpenedSubMenus?: string[],//默认展开的SubMenu
    mode?: MenuMode,
    style?: React.CSSProperties,
    onSelect?: selectCallback
}
interface ContextProps {
    mode?: MenuMode,
    currentIdx: string,
    defaultOpenedSubMenus?: string[],
    onSelect?: selectCallback
}
export const MenuContext = createContext<ContextProps>({currentIdx: '0'})

const Menu: React.FC<MenuProps> = (props) => {
    const { className, style, mode, children, defaultIndex, onSelect } = props;
    const classes = classNames('komorebi-menu', className, {
        'menu-vertical': mode === 'vertical',
        'menu-horizontal': mode !== 'vertical',
    })
    const [currentIdx, setCurrentIdx] = useState(defaultIndex);
    const handleMenuItemClick = (idx:string) => {
        setCurrentIdx(idx);
        onSelect && onSelect(idx);
    }
    const contextValue: ContextProps = {
        mode,
        currentIdx: currentIdx || '0',
        defaultOpenedSubMenus: ['3'],
        onSelect: handleMenuItemClick
    }
    const renderChildren = () => {
        return React.Children.map(children, (child, index) => {
            const childElement = child as React.FunctionComponentElement<MenuItemProps>;
            const { displayName } = childElement.type;
            if (displayName === 'MenuItem' || displayName === 'SubMenu') {
                return React.cloneElement(childElement, {
                    index: index.toString()
                })
            } else {
                console.error("Warning: Menu has a child which is not a MenuItem component")
            }
        })
    }
    return <ul className={classes} style={style} data-testid="test-menu">
        <MenuContext.Provider value={contextValue}>
            {renderChildren()}
        </MenuContext.Provider>    
    </ul>
}

Menu.defaultProps = {
    defaultIndex: '0',
    mode: 'horizontal'
}
export default Menu;