/*
 * @Author: wanghuang
 * @Date: 2021-09-26 14:02:30
 * @LastEditTime: 2021-09-27 16:35:42
 * @LastEditors: wanghuang
 * @Description: menu test
 */
import React from 'react';
import { render, fireEvent, RenderResult, cleanup, waitFor  } from "@testing-library/react";
import Menu, { MenuProps } from './menu';
import MenuItem from './menuItem';
import SubMenu from './subMenu';
jest.mock('react-transition-group', () => {
    return {
      CSSTransition: (props: any) => {
        return props.children
      }
    }
  })
const testProps: MenuProps = {
    defaultIndex: '0',
    className: 'test',
    onSelect: jest.fn(),
}
const testVerticalProps: MenuProps = {
    defaultIndex: '0',
    mode: 'vertical'
}
const generateMenu = (props: MenuProps) => {
    return <Menu {...props}>
        <MenuItem>嘿嘿</MenuItem>
        <MenuItem disabled>哈哈</MenuItem>
        <MenuItem>耶耶</MenuItem>
        <SubMenu title="dropdown">
            <MenuItem>drop1</MenuItem>
        </SubMenu>
    </Menu>
}
//动态给测试用例添加样式表
const createCssFile = () => {
    const cssFile: string = `
        .komorebi-submenu {
            display: none;
        }
        .komorebi-submenu.menu-opened {
            display: block;
        }
    `
    const style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = cssFile;
    return style;
}
let wrapper: RenderResult, menuElement: HTMLElement, activeElement: HTMLElement, disabledElement: HTMLElement;
describe('test Menu & MenuItem component', () => {
    //beforeEach钩子函数在下面每个case执行之前都会执行
    //用于执行一些公共的逻辑
    beforeEach(() => {
        wrapper = render(generateMenu(testProps))
        wrapper.container.append(createCssFile())
        menuElement = wrapper.getByTestId('test-menu')
        activeElement = wrapper.getByText('嘿嘿')//默认第一个menuItem被选中
        disabledElement = wrapper.getByText('哈哈')
    })
    it ('whether the menu and menuItem component based on the default props is rendered correctly',() => {
        expect(menuElement).toBeInTheDocument();
        expect(menuElement).toHaveClass('komorebi-menu test');
        //getElementsByTagName方法获取的元素不区分层级
        //expect(menuElement.getElementsByTagName('li').length).toEqual(3);
        //使用css的伪类:scope获取第一层的子元素
        expect(menuElement.querySelectorAll(':scope > li').length).toEqual(4);
        expect(activeElement).toHaveClass('menu-item is-active');
        expect(disabledElement).toHaveClass('menu-item is-disabled');
    })
    it ('whether the active value is normally set when clicking on a certain item', () => {
        const thirdItem = wrapper.getByText('耶耶');
        fireEvent.click(thirdItem);
        expect(thirdItem).toHaveClass('is-active');
        expect(activeElement).not.toHaveClass('is-active');//判断当点击第三项时，第一项的is-active样式是不是没了
        expect(testProps.onSelect).toHaveBeenCalledWith('2');//判断点击的索引值
        fireEvent.click(disabledElement);
        expect(disabledElement).not.toHaveClass('is-active');
        expect(testProps.onSelect).not.toHaveBeenCalledWith('1');
    }) 
    it('whether the vertical menu is rendered correctly when the value of mode is set to vertical', async() => {
        cleanup();//这里重新创建一wrapper,需要调用一下清理函数，不然dom节点上会有两个wrapper
        const wrapper = render(generateMenu(testVerticalProps));
        const menuElement = wrapper.getByTestId('test-menu');
        expect(menuElement).toHaveClass('menu-vertical');
    })
    it('whether show dropdown when the mouse hovers over the subMenu', () => {
        expect(wrapper.queryByText('drop1')).not.toBeVisible();
        const dropdownElement = wrapper.getByText('dropdown');
        fireEvent.mouseEnter(dropdownElement);
        //submenu有一个300ms的异步操作，而测试用例不会等待异步操作之后执行，这时候需要使用async/waitFor
        waitFor(() => {
            //等待异步操作执行完成之后再进行断言操作
            expect(wrapper.queryByText('drop1')).toBeVisible();
        })
        fireEvent.click(wrapper.getByText('drop1'));
        expect(testProps.onSelect).toHaveBeenCalledWith('3-0');
        fireEvent.mouseLeave(dropdownElement);
        waitFor(() => {
            expect(wrapper.queryByText('drop1')).not.toBeVisible();
        })
    })
})