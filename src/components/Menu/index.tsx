/*
 * @Author: wanghuang
 * @Date: 2021-09-16 15:23:34
 * @LastEditTime: 2021-09-25 12:48:47
 * @LastEditors: wanghuang
 * @Description: menu组件
 */
import Menu from './menu';

export default Menu;