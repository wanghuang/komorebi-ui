/*
 * @Author: wanghuang
 * @Date: 2021-09-27 10:54:59
 * @LastEditTime: 2021-09-28 17:26:58
 * @LastEditors: wanghuang
 * @Description: subMenu component
 */
import React, { useState, useContext } from "react";
import classNames from "classnames";
import { MenuContext } from "./menu";
import { MenuItemProps } from "./menuItem";
import Icon from "../Icon/icon";
import Transition from "../Transition";

export interface SubMenuProps {
    index?: string,
    title: string,
    className?: string
}

const SubMenu: React.FC<SubMenuProps> = (props) => {
    const { index, title, children, className } = props;
    const { currentIdx, defaultOpenedSubMenus, mode } = useContext(MenuContext);
    //defaultOpenedSubMenus是string[] | undefined的联合类型，不能直接调用组件方法，可以使用类型断言或者ES10的特性：可选链?操作
    //const isOpen = (index && mode === 'vertical') ? defaultOpenedSubMenus?.includes(index) : false;
    const openedSubMenus = defaultOpenedSubMenus as Array<string>;
    const isOpen = (index && mode === 'vertical') ? openedSubMenus.includes(index) : false;
    const [menuOpen, setMenuOpen] = useState(isOpen);
    const parentClasses = classNames('menu-item submenu-item', className, {
        'is-active': currentIdx === index,
        'is-opened': menuOpen,
        'is-vertical': mode === 'vertical'
    })
    const subMenuClass = classNames('komorebi-submenu', {
        'menu-opened': menuOpen
    })
    const handleClick = (e: React.MouseEvent) => {
        e.preventDefault();
        setMenuOpen(!menuOpen);
    }
    let timer: any;
    const handleHover = (e: React.MouseEvent, toggle: boolean) => {
        clearTimeout(timer);
        e.preventDefault();
        setTimeout(() => {
            setMenuOpen(toggle);
        }, 300);
    }
    const clickEvents = mode === 'vertical' ? { onClick: handleClick } : {};
    const hoverEvents = mode !== 'vertical' ? {
        onMouseEnter: (e: React.MouseEvent) => handleHover(e, true),
        onMouseLeave: (e: React.MouseEvent) => handleHover(e, false),
    } : {};
    const renderChildren = () => {
        const UlComponent = React.Children.map(children, (child, idx) => {
            const childElement = child as React.FunctionComponentElement<MenuItemProps>;
            const { displayName } = childElement.type;
            if (displayName === 'MenuItem') {
                return React.cloneElement(childElement, {
                    index: `${index}-${idx}`
                })
            } else {
                console.error("Warning: SubMenu has a child which is not a MenuItem component");
            }

        })
        return <Transition
            in={menuOpen}
            timeout={300}
            animation='zoom-in-top'
        >
            <ul className={subMenuClass}>
                {UlComponent}
            </ul>
        </Transition>
    }
    return <li key={index} className={parentClasses} {...hoverEvents}>
        <div className="submenu-title" {...clickEvents}>
            {title}
            <Icon icon="angle-down" className="arrow-icon"/>
        </div>
        {renderChildren()}
    </li>
}

SubMenu.displayName = 'SubMenu';
export default SubMenu;