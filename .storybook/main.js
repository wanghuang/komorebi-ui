/*
 * @Author: wanghuang
 * @Date: 2021-09-28 20:05:54
 * @LastEditTime: 2021-09-29 14:43:10
 * @LastEditors: wanghuang
 * @Description: 
 */
module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    //'@storybook/addon-docs',
    "@storybook/preset-create-react-app"
  ]
}