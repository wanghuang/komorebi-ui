/*
 * @Author: wanghuang
 * @Date: 2021-09-28 20:05:54
 * @LastEditTime: 2021-09-29 13:58:54
 * @LastEditors: wanghuang
 * @Description: 
 */

// 导入全局样式
import "../src/styles/index.scss";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}